# Copyright (C) 2016 The Pure Nexus Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# init.d script support
PRODUCT_COPY_FILES += \
    vendor/custom/prebuilt/etc/init.d/placeholder:system/etc/init.d/00placeholder

# Magisk
PRODUCT_COPY_FILES += \
    vendor/custom/prebuilt/etc/magisk.zip:system/addon.d/magisk.zip

# Pure-specific init file
#PRODUCT_COPY_FILES += \
#    vendor/custom/prebuilt/bin/launch_daemonsu.sh:root/sbin/launch_daemonsu.sh

# init.d script support
PRODUCT_COPY_FILES += \
    vendor/custom/prebuilt/bin/sysinit:system/bin/sysinit

ifneq ($(TARGET_BUILD_VARIANT),user)
# userinit support
PRODUCT_COPY_FILES += \
    vendor/custom/prebuilt/etc/init.d/90userinit:system/etc/init.d/90userinit
endif


ifneq ($(TARGET_BUILD_VARIANT),eng)
# Enable ADB authentication
ADDITIONAL_DEFAULT_PROPERTIES += ro.adb.secure=1
endif

ifneq ($(TARGET_BUILD_VARIANT),user)
ADDITIONAL_DEFAULT_PROPERTIES += ro.debuggable=0
endif


PRODUCT_PROPERTY_OVERRIDES += \
	rild.libpath=/vendor/lib64/libril-qc-qmi-1.so \
	ro.telephony.default_network=10 \
	telephony.lteOnCdmaDevice=1

# Default notification/alarm sounds
PRODUCT_PROPERTY_OVERRIDES += \
	ro.config.ringtone=Zen.ogg \
	ro.config.notification_sound=Chime.ogg \
	ro.config.alarm_alert=Flow.ogg


PRODUCT_PROPERTY_OVERRIDES += \
    ro.setupwizard.rotation_locked=true \
    persist.data.iwlan.enable=true \
    ro.storage_manager.enabled=true \
    ro.opa.eligible_device=true

# Prebuilt APKs
PRODUCT_PACKAGES += \
    ThemeInterfacer \
    Substratum \
    MagiskManager \
    KernelAdiutor

# Include explicitly to work around Facelock issues
PRODUCT_PACKAGES += \
    libprotobuf-cpp-full

# rootdir
PRODUCT_PACKAGES += \
    init.superxe.rc \
    Email \
    PureSettings 

# Prebuilt privileged APKs
PRODUCT_PACKAGES += \
    ViPER4Arise \
    GoogleDialer \
    com.google.android.dialer.support

# Symlinks
PRODUCT_PACKAGES += \
    libV4AJniUtils.so

# Security Enhanced Linux
#PRODUCT_PROPERTY_OVERRIDES += \
#    ro.build.selinux=1

