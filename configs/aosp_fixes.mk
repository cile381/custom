# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Google property overides
PRODUCT_PROPERTY_OVERRIDES += \
    ro.control_privapp_permissions=enforce \
    keyguard.no_require_sim=true \
    ro.url.legal=http://www.google.com/intl/%s/mobile/android/basic/phone-legal.html \
    ro.url.legal.android_privacy=http://www.google.com/intl/%s/mobile/android/basic/privacy.html \
    ro.com.google.clientidbase=android-google \
    ro.carrier=unknown \
    ro.error.receiver.system.apps=com.google.android.gms \
    ro.setupwizard.enterprise_mode=1 \
    ro.com.android.dataroaming=false \
    ro.atrace.core.services=com.google.android.gms,com.google.android.gms.ui,com.google.android.gms.persistent \
    ro.setupwizard.rotation_locked=true

# Proprietary latinime libs needed for Keyboard swyping
ifneq ($(filter shamu,$(TARGET_PRODUCT)),)
PRODUCT_COPY_FILES += \
    vendor/custom/prebuilt/lib/libjni_latinime.so:system/lib/libjni_latinime.so
else
PRODUCT_COPY_FILES += \
    vendor/custom/prebuilt/lib64/libjni_latinime.so:system/lib64/libjni_latinime.so
endif

PRODUCT_PROPERTY_OVERRIDES += \
	ro.config.ringtone=The_big_adventure.ogg \
	ro.config.notification_sound=Popcorn.ogg \
	ro.config.alarm_alert=Bright_morning.ogg

PRODUCT_PACKAGES += \
    Email 

PRODUCT_COPY_FILES += \
    vendor/custom/prebuilt/etc/permissions/com.google.android.dialer.support.xml:system/etc/permissions/com.google.android.dialer.support.xml

PRODUCT_COPY_FILES += \
    vendor/custom/prebuilt/etc/dialer_experience.xml:system/etc/sysconfig/dialer_experience.xml

#Pixel 2 Fonts
PRODUCT_COPY_FILES += \
    vendor/fonts/GoogleSans-Bold.ttf:system/fonts/GoogleSans-Bold.ttf \
    vendor/fonts/GoogleSans-BoldItalic.ttf:system/fonts/GoogleSans-BoldItalic.ttf \
    vendor/fonts/GoogleSans-Italic.ttf:system/fonts/GoogleSans-Italic.ttf \
    vendor/fonts/GoogleSans-Medium.ttf:system/fonts/GoogleSans-Medium.ttf \
    vendor/fonts/GoogleSans-MediumItalic.ttf:system/fonts/GoogleSans-MediumItalic.ttf \
    vendor/fonts/GoogleSans-Regular.ttf:system/fonts/GoogleSans-Regular.ttf \
    vendor/fonts/Roboto-BlackItalic.ttf:system/fonts/Roboto-BlackItalic.ttf \
    vendor/fonts/Roboto-Black.ttf:system/fonts/Roboto-Black.ttf \
    vendor/fonts/Roboto-BoldItalic.ttf:system/fonts/Roboto-BoldItalic.ttf \
    vendor/fonts/Roboto-Bold.ttf:system/fonts/Roboto-Bold.ttf \
    vendor/fonts/RobotoCondensed-BoldItalic.ttf:system/fonts/RobotoCondensed-BoldItalic.ttf \
    vendor/fonts/RobotoCondensed-Bold.ttf:system/fonts/RobotoCondensed-Bold.ttf \
    vendor/fonts/RobotoCondensed-Italic.ttf:system/fonts/RobotoCondensed-Italic.ttf \
    vendor/fonts/RobotoCondensed-LightItalic.ttf:system/fonts/RobotoCondensed-LightItalic.ttf \
    vendor/fonts/RobotoCondensed-Light.ttf:system/fonts/RobotoCondensed-Light.ttf \
    vendor/fonts/RobotoCondensed-Regular.ttf:system/fonts/RobotoCondensed-Regular.ttf \
    vendor/fonts/Roboto-Italic.ttf:system/fonts/Roboto-Italic.ttf \
    vendor/fonts/Roboto-LightItalic.ttf:system/fonts/Roboto-LightItalic.ttf \
    vendor/fonts/Roboto-Light.ttf:system/fonts/Roboto-Light.ttf \
    vendor/fonts/Roboto-MediumItalic.ttf:system/fonts/Roboto-MediumItalic.ttf \
    vendor/fonts/Roboto-Medium.ttf:system/fonts/Roboto-Medium.ttf \
    vendor/fonts/Roboto-Regular.ttf:system/fonts/Roboto-Regular.ttf \
    vendor/fonts/Roboto-ThinItalic.ttf:system/fonts/Roboto-ThinItalic.ttf \
    vendor/fonts/Roboto-Thin.ttf:system/fonts/Roboto-Thin.ttf

#$(call inherit-product, vendor/pixel/Android.mk)
$(call inherit-product, vendor/custom/sepolicy/sepolicy.mk)
#$(call inherit-product, frameworks/base/data/sounds/PixelSounds.mk)
$(call inherit-product, frameworks/base/data/sounds/Pixel2Sounds.mk)

