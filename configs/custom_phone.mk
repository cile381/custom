# Copyright (C) 2016 The Pure Nexus Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include vendor/custom/configs/aosp_fixes.mk
include vendor/custom/configs/bootanimation.mk
#include vendor/custom/configs/custom_main.mk
#include vendor/custom/configs/system_additions.mk
#include vendor/custom/configs/version.mk
#include vendor/custom/configs/viper.mk
#include vendor/prebuilt/prebuilt.mk

# Telephony packages
PRODUCT_PACKAGES += \
    Stk \
    CellBroadcastReceiver

# Include overlays
PRODUCT_PACKAGE_OVERLAYS += \
    vendor/custom/overlay

# Telephony packages
PRODUCT_PACKAGES += \
    busybox

# Main Required Packages
PRODUCT_PACKAGES += \
    LiveWallpapersPicker

PRODUCT_PACKAGES += \
    libprotobuf-cpp-full \
    librsjni

PRODUCT_PROPERTY_OVERRIDES += \
    debug.sf.hw=1 \
    debug.gralloc.enable_fb_ubwc=1

# Allow tethering without provisioning app
PRODUCT_PROPERTY_OVERRIDES += \
    net.tethering.noprovisioning=true \
    ro.opa.eligible_device=true \
    ro.boot.vendor.overlay.theme=com.google.android.theme.pixel

# Thank you, please drive thru!
PRODUCT_PROPERTY_OVERRIDES += \
    persist.sys.dun.override=0 \
    ro.storage_manager.enabled=true


PRODUCT_PROPERTY_OVERRIDES += \
    ro.build.selinux=1

PRODUCT_PROPERTY_OVERRIDES += \
    persist.sys.disable_rescue=true


